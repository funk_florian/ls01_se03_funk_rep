import java.util.Scanner;
public class Steuersatz {

	public static void main(String[] args) {
	
		double bruttobetrag = steuern();

		System.out.printf("Bruttobetrag: %.2f�" , bruttobetrag);
			
		
	}	
				
	public static double steuern() {
			Scanner tastatur = new Scanner(System.in);
			
			boolean istNichtKorrekt = false;
			double erg = 0;
			System.out.print("Nettowert: ");
			double nettowert = tastatur.nextDouble();
		
			do {
				System.out.print("Steuererm��igung:");
				char antwort = tastatur.next().charAt(0);
				
				
				if (antwort == 'j') {
					
					erg = nettowert * 1.16;
					istNichtKorrekt= false;
				}
				else if (antwort == 'n') {
					erg = nettowert * 1.19;
					istNichtKorrekt= false;
					
				}
				else {
					System.out.println("Fehler...bitte mit \"j\" oder \"n\" antworten!");
					istNichtKorrekt= true;
				}
			} while (istNichtKorrekt);
			
			
			return erg;
		}

	

}
