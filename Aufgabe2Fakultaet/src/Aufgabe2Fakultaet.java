import java.util.Scanner;

public class Aufgabe2Fakultaet {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		System.out.print("Bitte eine Zahl unter 20 eingeben: ");
		int n = tastatur.nextInt();
		if (n == 0) {
			System.out.println("0! = 1");
		}
		else {
			int fakultaetErgebnis = 1;
			int zaehler = 1;
			while (zaehler <= n) {
				fakultaetErgebnis = fakultaetErgebnis*zaehler;
				if (zaehler == n) {
					System.out.println(zaehler);
				}
				else {	
					System.out.print(zaehler + " * ");
				}
				zaehler ++;
			}
 
			System.out.println(n + "! = " + fakultaetErgebnis);

		}
	}

}
