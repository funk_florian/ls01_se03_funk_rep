import java.util.ArrayList;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	public Raumschiff() {
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffsname, int androidenAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.schiffsname = schiffsname;
		this.androidenAnzahl = androidenAnzahl;
	}

	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
	}

	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int zustandEnergieversorgungInProzentNeu) {
	}

	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}

	public void setSchildeInProzent(int zustandSchildeInProzentNeu) {
	}

	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}

	public void setHuelleInProzent(int zustandHuelleInProzentNeu) {
	}

	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int zustandLebenserhaltungssystemeInProzentNeu) {
	}

	public String getSchiffsname() {
		return this.schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
	}

	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
	}

	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);

	}

	public void photonentorpedoSchiessen(Raumschiff r) {
		if (this.photonentorpedoAnzahl == 0) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			this.photonentorpedoAnzahl--;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(r);
		}

	}

	public void phaserkanoneSchiessen(Raumschiff r) {
		if (this.energieversorgungInProzent < 50) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			this.energieversorgungInProzent -= 50;
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffer(r);
		}
	}

	private void treffer(Raumschiff r) {
		System.out.println(r.schiffsname + " wurde getroffen!");
	}

	public void nachrichtAnAlle(String message) {
		this.broadcastKommunikator.add(message);
		System.out.println(message);
	}

	public String[] eintraegeLogbuchZurueckgeben() {
		String[] antwort = new String[2];
		return antwort;
	}

	public void photonentorpedosLaden(int anzahlTorpedos) {

	}

	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {

	}

	public void zustandRaumschiff() {

		System.out.println("Photonentorpedos: " + this.photonentorpedoAnzahl);
		System.out.println("Energie:" + this.energieversorgungInProzent);
		System.out.println("Schilde: " + this.schildeInProzent);
		System.out.println("H�lle: " + this.huelleInProzent);
		System.out.println("Lebenserhaltungssysteme: " + this.lebenserhaltungssystemeInProzent);
		System.out.println("Androiden:" + this.androidenAnzahl + "\n");
	}

	public void ladungsverzeichnisAusgeben() {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			System.out.println(ladungsverzeichnis.get(i));
		}
		System.out.println("");
	}

	public void ladungsverzeichnisAufraeumen() {

	}
}
