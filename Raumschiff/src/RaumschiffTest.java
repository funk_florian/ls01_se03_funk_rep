
public class RaumschiffTest {

	public static void main(String[] args) {

		Raumschiff Klingonen = new Raumschiff(1, 100, 100, 100, 100, "IKS Hegh�ta", 2);
		Raumschiff Romulaner = new Raumschiff(2, 100, 100, 100, 100, "IRW Khazara", 2);
		Raumschiff Vulkanier = new Raumschiff(0, 80, 80, 50, 100, "Ni�Var", 5);

		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Borg-Schrott", 5);
		Ladung l3 = new Ladung("Rote Materie", 2);
		Ladung l4 = new Ladung("Forschungssonde", 35);
		Ladung l5 = new Ladung("Bat�leth Klingonen Schwert", 200);
		Ladung l6 = new Ladung("Plasma-Waffe", 50);
		Ladung l7 = new Ladung("Photonentorpedo", 3);

		Klingonen.addLadung(l1);
		Klingonen.addLadung(l5);
		Romulaner.addLadung(l2);
		Romulaner.addLadung(l3);
		Romulaner.addLadung(l6);
		Vulkanier.addLadung(l4);
		Vulkanier.addLadung(l7);

		/*
		 * Klingonen.zustandRaumschiff();
		 * Romulaner.zustandRaumschiff();
		 * Vulkanier.zustandRaumschiff();
		 * 
		 * Klingonen.ladungsverzeichnisAusgeben();
		 * Romulaner.ladungsverzeichnisAusgeben();
		 * Vulkanier.ladungsverzeichnisAusgeben();
		 * 
		 * Klingonen.photonentorpedoSchiessen(Romulaner);
		 * Vulkanier.photonentorpedoSchiessen(Romulaner);
		 * 
		 */
	}

}
