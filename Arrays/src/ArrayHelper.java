
public class ArrayHelper {

	public static void main(String[] args) {
		int[] zahlenliste = { 1, 2, 3, 4, 5 };

		System.out.println(convertArrayToString(zahlenliste));

	}

	public static String convertArrayToString(int[] zahlen) {
		String arrayToString = "[ ";
		for (int i = 0; i < zahlen.length; i++) {
			arrayToString += zahlen[i] + " ";
		}
		arrayToString += "]";
		return arrayToString;
	}

}
