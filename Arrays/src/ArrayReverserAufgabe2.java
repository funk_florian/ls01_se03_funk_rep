import java.util.Arrays;

public class ArrayReverserAufgabe2 {

	public static void main(String[] args) {
		int[] zahlennliste = { 1, 2, 3, 4, 5};

		System.out.println(Arrays.toString(zahlennliste));
		reverseArray1(zahlennliste);		
		System.out.println(Arrays.toString(zahlennliste));
	}

	
	public static void reverseArray1(int[] zahlen) {
		if (zahlen != null) {
			int temp = 0;
			for (int i = 0; i < zahlen.length / 2; i++) {
				temp = zahlen[i];
				zahlen[i] = zahlen[zahlen.length - 1 - i];
				zahlen[zahlen.length - 1 - i] = temp;
			}
		}
	}
}