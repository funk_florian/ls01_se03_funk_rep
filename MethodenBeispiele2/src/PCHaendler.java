import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		String artikel = liesString("Was m�chten Sie bestellen?");  
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		double nettoPreis = liesDouble("Geben Sie den Nettopreis ein:");
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		
		
		double nettoGesamtpreis = berechneGesamtnettopreis(anzahl,nettoPreis); 
		double bruttoGesamtpreis = berechneGesamtbruttopreis(nettoGesamtpreis,mwst);
		
		rechnungausgeben(artikel,anzahl,nettoGesamtpreis,bruttoGesamtpreis,mwst);
		
		
	}	
		public static String liesString(String text) {
			Scanner myScanner = new Scanner(System.in);
			
			System.out.println(text);
			
			String str = myScanner.next();
			return str;	
		}
		
		public static int liesInt(String text) {
			Scanner myScanner = new Scanner(System.in);
			
			System.out.println(text);
			
			int zahl = myScanner.nextInt();
			return zahl;
			}

		public static double liesDouble(String text) {
			Scanner myScanner = new Scanner(System.in);
			
			System.out.println(text);
			
			double zahl = myScanner.nextDouble();
			return zahl;
			
			
		}
		
		
		public static double berechneGesamtnettopreis(int anzahl, double nettoPreis) {
			
			double gesamtNetto = anzahl * nettoPreis;
			return gesamtNetto;
		}
		
		public static double berechneGesamtbruttopreis(double nettoGesamtpreis, double mwst) {
		
			double gesamtBrutto = nettoGesamtpreis * (1+ mwst / 100);
			return gesamtBrutto;
		}
		
		public static void rechnungausgeben(String artikel, int anzahl, double nettoGesamtpreis, double bruttoGesamtpreis, double mwst) {
			
			System.out.println("\tRechnung");
			System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettoGesamtpreis);
			System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttoGesamtpreis, mwst, "%");	
			
		}
		
		/* Benutzereingaben lesen
		System.out.println("Was m�chten Sie bestellen?");
		String artikel = myScanner.next();

		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = myScanner.nextInt();

		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = myScanner.nextDouble();

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();

		// Verarbeiten
		double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	    */	
	

}
