import java.util.Scanner;

public class SchleifenBeispiele {

	public static void main(String[] args) {
	
		Scanner myScanner = new Scanner(System.in);

		System.out.print("Bitte den Startwert in Celsius eingeben: ");
		int startwertCelsius = myScanner.nextInt();
		
		System.out.print("Bitte den Endwert in Celsius eingeben: ");
		int endwertCelsius = myScanner.nextInt();
		
		System.out.print("Bitte den Schrittweite in Celsius eingeben: ");
		int schrittweite = myScanner.nextInt();

		int zaehler = startwertCelsius;
		
		while (zaehler <= endwertCelsius) {
			
			double farenheit = 32 + (zaehler * 1.8);
			
			if (zaehler == endwertCelsius) {
				System.out.println(zaehler + " C�      " + farenheit + " F�");
				
			}
			else {
				System.out.println(zaehler + " C�      " + farenheit + " F�" + ",");
			}
			
				zaehler = (zaehler + schrittweite);
			
		}
	}

}
