
public class Aufgabe3 {

	public static void main(String[] args) {
		String f = "Fahrenheit";
		String c = "Celsius";
		System.out.printf("%-11s|", f);
		System.out.printf("%9s %n", c );
		System.out.println("----------------------");
		
		System.out.printf("%-11s|%9.2f %n", "-20" , -28.8889 );
		System.out.printf("%-11s|%9.2f %n", "-10" , -23.3333 );
		System.out.printf("%-11s|%9.2f %n", "+0" , -17.7778 );
		System.out.printf("%-11s|%9.2f %n", "+20" , -6.6667 );
		System.out.printf("%-11s|%9.2f %n", "+30" , -1.1111 );
		
		/*
		System.out.printf("%-11s|", "Fahrenheit");
		System.out.printf("%9s %n", "Celsius" );
		System.out.println("----------------------");
		System.out.printf("%-11s|", "-20");
		System.out.printf("%9.2f %n", -28.8889 );
		System.out.printf("%-11s|", "-10");
		System.out.printf("%9.2f %n", -23.3333 );
		System.out.printf("%-11s|", "+0");
		System.out.printf("%9.2f %n", -17.7778 );
		System.out.printf("%-11s|", "+20");
		System.out.printf("%9.2f %n", -6.6667 );
		System.out.printf("%-11s|", "+30");
		System.out.printf("%9.2f %n", -1.1111 );
		*/

	}

}
