
public class MethodeBeispiel {

	public static void main(String[] args) {
		
		
		/*
		sayHello("Max");
		sayHello();	
		
		add(2,3);
		
		addDouble(3.5,7.7);
		
		summe(3,6,7);
		*/
		
		berechnePreis(3,10.50f,"Monitore");
		
	}
	/*

	public static void sayHello() {
		System.out.println("hello...");
	}
	
	public static void sayHello(String name) {
		System.out.println("hello " + name);
	}
	
	public static void add(int zahl1, int zahl2) {
		int erg = zahl1 + zahl2;
		System.out.println(zahl1 + "+" + zahl2 + "=" + erg);
	}
	
	
	public static void addDouble(double zahl1, double zahl2) {
		double erg = zahl1 + zahl2;
		System.out.println("Zahl1: " + zahl1 + "\nZahl2: " + zahl2 + "\nErgebnis: " + erg);
	}
	
	public static void summe(int zahl1, int zahl2, int zahl3){
		int erg = zahl1 + zahl2 + zahl3;
		
		System.out.println(zahl1 + " + " + zahl2 + " + " + zahl3 + " = " + erg);
		
		
	}
	*/
	
	public static void berechnePreis(int anzahl, float preis, String monitor){
		float erg = anzahl * preis;
		
		System.out.printf("%d %s kosten %.2f %n" , anzahl , monitor , erg);		
		
	}
	
}
