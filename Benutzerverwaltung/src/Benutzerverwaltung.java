import java.util.ArrayList;
import java.util.Scanner;

public class Benutzerverwaltung {
	public static ArrayList<Benutzer> benutzerliste = new ArrayList<Benutzer>();

	public static void main(String[] args) {

		int auswahl;

		do {
			auswahl = menu();
			switch (auswahl) {
			case 1:
				benutzerAnzeigen();
				break;
			case 2:
				bentzerErfassen();
				break;
			case 3:
				benutzerLöschen();
				break;
			case 4:
				System.exit(0);
			default:
				System.err.println("\nFalsche Eingabe.\n");
			}
		} while (true);

	}

	public static void benutzerAnzeigen() {
		for (int i = 0; i < benutzerliste.size(); i++) {
			System.out.println(benutzerliste.get(i));
		}
	}

	public static void bentzerErfassen() {
		Scanner input = new Scanner(System.in);
		System.out.print("Bitte Nummer eingeben: ");
		int benutzernummer = input.nextInt();
		System.out.print("Bitte Name eingeben: ");
		String name = input.next();
		Benutzer b1 = new Benutzer(benutzernummer, name);
		benutzerliste.add(b1);

	}

	public static void benutzerLöschen() {
		Scanner input = new Scanner(System.in);
		System.out.print("Welcher Benutzer soll gelöscht werden?: ");
		
		benutzerliste.remove(input.nextInt());

	}

	public static int menu() {

		int selection;
		Scanner input = new Scanner(System.in);

		/***************************************************/

		System.out.println("     Benutzerverwaltung     ");
		System.out.println("----------------------------\n");
		System.out.println("1 - Benutzer anzeigen");
		System.out.println("2 - Benutzer erfassen");
		System.out.println("3 - Benutzer löschen");
		System.out.println("4 - Ende");

		System.out.print("\nEingabe:  ");
		selection = input.nextInt();
		return selection;
	}

}
