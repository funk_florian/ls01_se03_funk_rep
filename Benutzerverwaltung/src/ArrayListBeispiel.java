import java.util.ArrayList;

public class ArrayListBeispiel {

	public static void main(String[] args) {
		/*
		 * String str1; String str2; //String str...; String str1000;
		 */
		String[] sliste = new String[2];

		sliste[0] = "Max";
		sliste[1] = "Anna";

		ArrayList<String> strList = new ArrayList<String>();

		strList.add("Max");
		strList.add("Anna");
		strList.add("Theo");
		strList.add("Klaus");
		strList.add("Mona");
		strList.add("Jens");
		System.out.println(strList);

		strList.remove("Max");
		System.out.println(strList);

	}

}
