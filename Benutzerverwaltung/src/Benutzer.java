
public class Benutzer {
	private int benutzernummer;
	private String name;

	public Benutzer(int benutzernummer, String name) {
		this.benutzernummer = benutzernummer;
		this.name = name;
	}

	public int getBenutzernummer() {
		return this.benutzernummer;
	}

	public void setBenutzernummer(int benutzernummer) {
		this.benutzernummer = benutzernummer;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return "(Benutzernummer: " + this.benutzernummer + " , Name: " + this.name + ")";

	}
}
